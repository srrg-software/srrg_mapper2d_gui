#include "simple_graph_viewer.h"

namespace srrg_mapper2d_gui {

  SimpleGraphViewer::SimpleGraphViewer(SimpleGraph* sgraph_) {
    _sgraph=sgraph_;

    _draw_covariances = true;
    _draw_clouds = true;
  }

  void SimpleGraphViewer::draw(){
    if (!_sgraph)
      return;

    //Draw vertices
    drawVertices();
    //Draw clouds
    drawClouds();
    //Draw edges
    drawEdges();

    if (_draw_covariances)
      //Draw covariances
      drawCovariances();
  }

  void SimpleGraphViewer::drawEdges(){
    if (!_sgraph)
      return;
    for (OptimizableGraph::EdgeSet::iterator it=_sgraph->graph()->edges().begin(); it!=_sgraph->graph()->edges().end(); ++it) {
      EdgeSE2* e = dynamic_cast<EdgeSE2*>(*it);
      if (e)
	drawEdge(e);
    }
  }

  void SimpleGraphViewer::drawEdge(EdgeSE2* e) {
    VertexSE2* vfrom = (VertexSE2*) e->vertices()[0];
    VertexSE2* vto   = (VertexSE2*) e->vertices()[1];

    glPushAttrib(GL_COLOR|GL_POINT_SIZE);
    glColor3f(1,0,0);
    glPointSize(1);
    glBegin(GL_LINES);
    glNormal3f(0,0,1);
    glVertex3f(vfrom->estimate().translation().x(), vfrom->estimate().translation().y(), 0);
    glNormal3f(0,0,1);
    glVertex3f(vto->estimate().translation().x(), vto->estimate().translation().y(), 0);
    glEnd();
    glPopAttrib();
  }


  void SimpleGraphViewer::drawClouds(){
    if (!_sgraph)
      return;    
    for (OptimizableGraph::VertexIDMap::iterator it=_sgraph->graph()->vertices().begin(); it!=_sgraph->graph()->vertices().end(); ++it) {
      VertexSE2* v = dynamic_cast<VertexSE2*>(it->second);
      if (v){
	Cloud2DWithTrajectory* cloudTrajectory = dynamic_cast<Cloud2DWithTrajectory*>(_sgraph->vertexCloud(v));
	if (cloudTrajectory)
	  cloudTrajectory->draw(false, false, _draw_clouds);	
	else{
	  Cloud2DWithPose* cloudPose = dynamic_cast<Cloud2DWithPose*>(_sgraph->vertexCloud(v));
	  if (cloudPose)
	    cloudPose->draw();
	}
      }
    }
  }


  void SimpleGraphViewer::drawVertices(){
    if (!_sgraph)
      return;
    for (OptimizableGraph::VertexIDMap::iterator it=_sgraph->graph()->vertices().begin(); it!=_sgraph->graph()->vertices().end(); ++it) {
      VertexSE2* v = dynamic_cast<VertexSE2*>(it->second);
      if (v)
	drawVertex(v);
    }
  }

  void SimpleGraphViewer::drawVertex(VertexSE2* v){
    glPushAttrib(GL_COLOR|GL_POINT_SIZE);
    glPointSize(5.0f);
    glColor3f(1,0,0);
    glBegin(GL_POINTS);
    glVertex3f(v->estimate().translation().x(), v->estimate().translation().y(), 0.0f);
    glEnd();
    glPopAttrib();
  }

  void SimpleGraphViewer::drawCovariances(){
    if (!_sgraph)
      return;
    for (OptimizableGraph::VertexIDMap::iterator it=_sgraph->graph()->vertices().begin(); it!=_sgraph->graph()->vertices().end(); ++it) {
      VertexSE2* v = dynamic_cast<VertexSE2*>(it->second);
      if (v)
	drawCovariance(v);
    }
  }

  void SimpleGraphViewer::drawCovariance(VertexSE2* v){

    VertexEllipse* ellipse = _sgraph->findEllipseData(v);

    if (ellipse){
      glPushMatrix();

      glTranslatef(v->estimate().translation().x(), v->estimate().translation().y(), 0);
      glRotatef(180.0f/M_PI*v->estimate().rotation().angle(), 0,0,1);

      float sigmaTheta = sqrt(ellipse->covariance()(2,2));
      float x = 0.1*cos(sigmaTheta);
      float y = 0.1*sin(sigmaTheta);

      glColor3f(1.f,0.5f,1.f);
      glBegin(GL_LINE_STRIP);
      glVertex3f(x,y,0);
      glVertex3f(0,0,0);
      glVertex3f(x,-y,0);
      glEnd();
       
      glColor3f(0.f,1.f,0.f);
      for (int i=0; i< ellipse->matchingVertices().size(); i++){
	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(ellipse->matchingVertices()[i].x(),ellipse->matchingVertices()[i].y(),0);
	glEnd();
      }

      Eigen::Matrix2f rot = ellipse->U();
      float angle = atan2(rot(1,0), rot(0,0));
      glRotatef(angle*180.0/M_PI, 0., 0., 1.);
      Eigen::Vector2f sv = ellipse->singularValues();
      glScalef(2*sqrt(sv(0)), 2*sqrt(sv(1)), 1);

      glColor3f(1.f,0.5f,1.f);
      glBegin(GL_LINE_LOOP);
      for(int i=0; i<36; i++){
	float rad = i*M_PI/18.0;
	glVertex2f(cos(rad),
		   sin(rad));
      }
      glEnd();

      glPopMatrix();
    }

  }

  void SimpleGraphViewer::setColorGraph(const Eigen::Vector3f& color){
    for (VertexSE2Cloud2DMap::iterator it = _sgraph->verticesClouds().begin(); it != _sgraph->verticesClouds().end(); it++){
      Cloud2DWithPose* c = it->second;
      c->setColor(color);
    }
  }

}
