#pragma once

#include "simple_graph_viewer.h"

#include <QGLViewer/manipulatedCameraFrame.h>

namespace srrg_mapper2d_gui {
  class InteractiveGraphViewer: public SimpleGraphViewer {
  public:

    InteractiveGraphViewer(SimpleGraph* sgraph_ = 0);

    inline void setMovableCloud(Cloud2DWithPose* movable_cloud_){_movable_cloud = movable_cloud_;}
    Cloud2DWithPose* movableCloud(){return _movable_cloud;}
    inline void removeMovableCloud() {delete _movable_cloud; _movable_cloud = 0;}
    void updateCloudPose();

    void init();

    void postSelection(const QPoint&);
    
    void draw();
    void drawWithNames();

  protected:
    Cloud2DWithPose* _movable_cloud;
    qglviewer::ManipulatedFrame* _cloud_frame;
    bool _move_managed;
  };
  
}
