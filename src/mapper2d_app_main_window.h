#pragma once

//Qt includes
#include <QApplication>
#include <QWidget>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QGroupBox>
#include <QPushButton>
#include <QFileDialog>
#include <QString>
#include <QLineEdit>
#include <QFileInfo>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QtGlobal>

//srrg includes
#include "srrg_messages/laser_message.h"
#include "srrg_messages/message_reader.h"

#include "tracker_viewer.h"
#include "interactive_graph_viewer.h"
#include "mapper2d.h"
#include "laser_message_tracker_params.h"
#include "mapper2d_params.h"

using namespace srrg_mapper2d_gui;
using namespace srrg_scan_matcher_gui;

class Mapper2DAppMainWindow: public QWidget {
public:

  Mapper2DAppMainWindow();
  
  inline void setTrackerInViewer(srrg_scan_matcher::Tracker2D* tracker_){tviewer->setTracker(tracker_);}
  inline void setGraphInViewer(SimpleGraph* sgraph_){gviewer->setGraph(sgraph_);}
  inline void setMapper(Mapper2D* mapper_){mapper = mapper_;}
  inline Tracker2DViewer* trackerViewer(){return tviewer;}
  inline InteractiveGraphViewer* graphViewer(){return gviewer;}

  void selectLog();
  void init();
  void run();
  void pause();
  void stop();
  void save();
  void selectMap();
  void removeMap();

  inline void setTrackerParams(LaserMessageTrackerParams* trackerparams_) {trackerparams = trackerparams_;}
  inline void setMapperParams(Mapper2DParams* mapperparams_) {mapperparams = mapperparams_;}
  void trackerParamsWindow();
  void mapperParamsWindow();

  void countMessages();

private:
  QGridLayout* glayout;
  QVBoxLayout* vlayout_l;
  QVBoxLayout* vlayout_r;
  QVBoxLayout* vlayout_ms;
  QHBoxLayout* hlayout_mapperview;
  QHBoxLayout* hlayout_params;
  QHBoxLayout* hlayout_load;
  QHBoxLayout* hlayout_run;
  QHBoxLayout* hlayout_prevload;  
  
  QLabel* label_tracker;
  QLabel* label_trackerparams;
  QLabel* label_mapper;
  QLabel* label_mapperparams;
  QLabel* label_curlog;
  QLabel* label_prevlog;
  QLabel* label_status;
  QLabel* label_drawclouds;
  QLineEdit* lineedit_curlog;
  QLineEdit* lineedit_prevlog;
  
  QGroupBox* gb_current_session;
  QGroupBox* gb_previous_session;
  QVBoxLayout* vlayout_cs;
  QPushButton* button_trackerparams;
  QPushButton* button_mapperparams;
  QPushButton* button_load;
  QPushButton* button_start;
  QPushButton* button_pause;
  QPushButton* button_stop;
  QPushButton* button_save;
  QPushButton* button_prevload;
  QPushButton* button_prevremove;
  QCheckBox* value_drawclouds;
  
  //Viewers
  Tracker2DViewer* tviewer;
  InteractiveGraphViewer* gviewer;

  //Log reader
  std::string logfilename;
  MessageReader* reader;
  BaseMessage* msg;

  //Mapper variables
  Mapper2D* mapper;

  //Parameters
  LaserMessageTrackerParams* trackerparams;
  Mapper2DParams* mapperparams;
  
  //Status variables
  bool paused;
  bool have_log;
  bool multi_session;
  int total_count;
  int current_count;
  
};
